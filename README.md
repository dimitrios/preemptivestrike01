# README

## Purpose

We decided to open source this project in order to make it easier for people to quickly build personal blogs or fansites for their favourite band.

We have let the original content in so you can do any dev/design work with actual content.

If you need any assistance / customization e-mail Dimitrios: `dimitrios@mistriotis.com`

## Forking and configuring the project from Gitlab

When you fork the project from Gitlab to create your own website please make sure to change your project name and your url name within the advanced settings:

Settings → General, then expand "Advanced" and change (1) "Project name" and (2) "Path" to something related to your project

## Getting started

```bash
bundle install
bundle exec jekyll s
```

This project acts like any other Jekyll site. You can find extensive instructions on how to get started with Jekyll at:

```
https://jekyllrb.com/docs/quickstart/
```

## Add/Update content

### Adding Interviews/Reviews

1. You can find Interviews & Reviews within the "posts" folder
2. Duplicate an existing interview or review file
3. New file name should have date (US system, e.g 2018-08-22) followed by post title
4. Replace markdown content with content of new post
5. ???
6. Profit

### Change Background Picture

1. Place image file in "assets" folder
2. In "sass/particles/base.scss" on line 17, change the image path to the new image file you added

## Setup domain

For Gitlab:
<https://docs.gitlab.com/ce/user/project/pages/getting_started_part_three.html>

## File Structure

TODO

## Known Issues

1.  Permalinks need to end in ".html" when using Gitlab pages.

## License

See `License.txt`.

---
layout: posts
title:  'Interview in "Mithra! temple zine"'
date:   10-06-2016
categories: Interview
permalink: /interviews/interview-in-mithra-temple-zine.html
---
Published in two languages, French and English.&nbsp;Jim the Blaster talks about his industrial black metal project PUNISHMENT SYSTEMS and reveals to his fans his favourite black metal bands!<br><br>

English version: <a href="http://mithratemplezine.tumblr.com/post/130486727184/interview-preemptive-strike-10" target="_blank">http://mithratemplezine.tumblr.com/post/130486727184/interview-preemptive-strike-10</a><br><br>

French version: <a href="http://mithratemplezine.tumblr.com/post/130486716874/entretien-preemptive-strike-10" target="_blank">http://mithratemplezine.tumblr.com/post/130486716874/</a>

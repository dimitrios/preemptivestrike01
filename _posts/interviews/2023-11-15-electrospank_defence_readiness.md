---
layout: posts
title: "Defense Readiness: Condition 1 Interview, ElectroSpank"
date: 15-11-2023
categories: Interview
permalink: /interviews/electrospank_defence.html
---

> Being an industrial/EBM music lover on a Greek island can feel pretty lonely as most people living there, are complete ignorants of these genres.

> However, when the fire of creating music burns within, one can be nothing but unstoppable. This is the case of PreEmptive Strike 0.1. A band that for 21 years has been fighting the status quo,trying to flourish in a soil that lacks the necessary ingredients for its growth. A project that has had some glorious times,but has also been through thin and thick, undergoing significant changes in its consistency,but guess what? It continues to find its way to success,proving that deep love and devotion finally pays off.

> 2023 finds Preemptive Strike 0.1 making the long-desired and striking restart by returning to their home Label , the German Infarcted Recordings. "Marauders from Earth" was the first single out from their 8th album "Defense Readiness: Condition 1" released on October 27th and the journey back to the top is set off.

Full Interview: <https://elektrospank.com/interviews/549-preemptive-strike-0-1-sci-fi-horror-themed-dark-electro-industrial-interview>

---
layout: posts
title:  'A different shade of black Metalzine'
date:   25-10-2014
categories: Interview
permalink: /interviews/a-different-shade-of-black-metalzine-preemptive-strike-01-interview.html
---

![The Band](/images/9055610_orig.jpg)

1.Can you give us an update on what has been going on with the band since the recording and release of the new ep?

JIM: We have just finished our new pure electronic CD(the 5th one) that will be released by the famous electronic label INFACTED RECORDINGS once again.

But we will have a collaboration with the Polish old school black metal band BLACK ALTAR. This will be a hard, guitar-driven track that will surely interest the extreme metal fans that read this interview.

Besides this we have done a cover of a well-known song of the power metal band SABATON!

More: [http://darkdoomgrinddeath.blogspot.co.uk/2014/10/preemptive-strike-01-interview.html](http://darkdoomgrinddeath.blogspot.co.uk/2014/10/preemptive-strike-01-interview.html)

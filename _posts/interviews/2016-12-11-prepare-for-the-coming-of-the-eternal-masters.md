---
layout: posts
title:  "Prepare for the coming of the eternal masters"
date:   11-12-2016
categories: Interview
permalink: /interviews/interview-on-wwwreshimotohucom-prepare-for-the-coming-of-the-eternal-masters.html
---

![Reshimotohu](/images/logos/reshimotohu-logo_orig.png)

"Preemptive What?

PES 0.1 can easily be listed among my top 10 musical crushes of 2015. Although originally they do not come from the dark realms of Black Metal, which as many of you know, is my comfortable natural habitat, you still cannot deny the connection between PES and Black Metal, as imminent from their latest effort, Eternal Masters. ..."

[Read the full interview here](http://www.reshimotohu.com/interviews/#/preemptive-strike-01-interview-english/)

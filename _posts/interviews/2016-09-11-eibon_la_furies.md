---
layout: posts
title:  'Mini Interview with Paul from Eibon la Furies about the collaboration on "Eternal Masters" '
date:   11-09-2016
categories: Interview
permalink: /interviews/mini-interview-with-paul-from-eibon-la-furies-about-the-collaboration-on-eternal-masters.html
---
<img src="/images/logos/eibon-la-furies_orig.png"/><br>

**Hi Paul, describe your band to someone who just read your name next to a PS01 track, knowing nothing about you.**

Eibon la Furies is an eccentric, avant-garde black metal band from the UK with lyrics based around Victorian occult ideologies and the darker aspects of human thought.

**How did you get to know PS01, what led to your collaboration? Why did you chose to work on "PLANET ERADICATED"?**

PS01 approached us with a tight deadline to collaborate with them but we're always up for a challenge! When we heard the track, PLANET ERADICATED, we knew our styles would work well together. The extreme, staccato guitars, slightly unhinged choirs and acerbic vocals of Eibon la Furies worked perfectly with the rhythmic industrial assault and dark electronics of PS01. Hopefully we brought a little British eccentricity to a great industrial piece by PreEmptive Strike 0.1 and are looking forward to the whole release. We would certainly work with PS01 again as they have a very professional yet friendly attitude to collaborative working. Thank you very much guys for inviting us along for this collaboration!

**Which is your opinion about crossovers between bands of different genres? Have you tried it in the past, will you try again in the future?**

Crossovers done well are awesome - creativity should push music outside boundaries - so it's always interesting to see what happens. Actually early Eibon la Furies had a slight industrial edge to it before morphing into a live band - tracks like, And By The Moonlight, for example, in it's original form was pretty much our version of Industrial Black Metal, so working on PLANET ERADICATED and its intense fast pace was almost a feeling of coming home! As for will we in the future, of course, anyone has to just ask and we'll approach everything on its own merit (and probably say yes!!!).

**Where can someone learn more about you and your work?**

You can find out about ELF in the usual places: <a target="_blank" href="http://www.facebook.com/eibonlafuries">www.facebook.com/eibonlafuries</a>, official website <a target="_blank" href="http://www.eibonlafuries.co.uk">www.eibonlafuries.co.uk</a>, label site <a target="_blank" href="http://www.code666.net">www.code666.net</a> or search for <a target="_blank" href="https://www.youtube.com/results?search_query=Eibon+la+Furies">Eibon la Furies on YouTube</a>.

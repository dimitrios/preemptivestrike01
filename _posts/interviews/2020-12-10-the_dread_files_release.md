---
layout: posts
title: "The Dread Files post Release Interview"
date: 10-12-2020
categories: Interview
permalink: /interviews/the_dread_files.html
---

<img src="/images/the_dread_files/band.jpg" style="width:390;max-width:100%">

**Let's start with the follow ups from the last release, "Progeny of the Technovore", how was it received and how do you feel about it looking back? I feel, and please correct me if wrong, that media response was a bit more quiet than usual.**

**Jim**: "Progeny of the Technovore" had some good reviews praising use because we did something different which combined Dark Electro/EBM with Goa Trance, it also got a place in a few charts for some weeks so I will disagree with your statement... Now if you want my opinion I would not do again a release with similar sound as it is not one of my most favourite genres as a fan. I lean towards old school dark electro, and specially in the last two years more towards retro-"worshipping" - these as a fan.

Additionally for 3 months we did 4 lives to promote it, two of them outside Greece, something which had not happened in the past; I really enjoyed/loved it after it while listening to some specific songs these moments come back to my mind making me feel very nostalgic...


**What are your thoughts on the album looking back. For me I enjoy it a lot but do not have that many tracks that I could come back to.**

**Jim**: In either case some tracks will become what we say "classics" and will be performed any of our live performances in the future such as "Constriction Process", "Xenobiological Exodeity", and of course my most loved "Tactical nuke incoming". I will agree with you, probably you refer to tracks not having memorable refrains having more "abstract" trance structures, although I am not responsible for this... I have to add that it had an amazing cover and corresponding artwork in general.


<img src="/images/the_dread_files/band_close.jpg" style="width:390;max-width:100%">

**And after "Progeny" got released, one of the oldest members, George, left the band. Was that decided before the release - this album and I am done - or it happened afterwards?**

**Jim**: It was his decision which was made on September of 2019 when the album was completed, handled over to the company - with pre-sales having stared - but not yet officially released in the pressing stage. We could not continue together as we had many disagreements from 2009, we had chosen different ways to live, and had different life priorities. We had reached a dead-end where we were not being called to play live. George did not like lives, preferred to sit at home and write music instead of getting up on stage. Hence not having any live offers was a "comfortable" if not ideal situation for him. Me on the other hand would never like to compose music without reason or purpose, but rather also present what I worked on to fans on-stage. And oh! as if from a miracle immediately after him leaving and releasing the album we got 4 live offers - those I mentioned before - which made me feel what we call "happiness" both as a musician and as a person...


**Was it brewing or there were heated moments?**

**Jim**: Up to interpretation. We had a heated discussion and after that we just do not talk any more. Who is right or wrong is better to be judged by an outsider. Issues in the group were in place since 2009 as I already told. In 2013 he had left the group for 4 months for the same reasons: being pressured in his personal life.. As a musician he is great, but is this enough? I am afraid not... As individuals we could not coexist having completely opposite angles on things. If you ask people where we live I will always be the "bad" guy and George the "good" guy, this from people living in the area, now if you see it from the "what needs to be done to have a serious EBM/Industrial band" such as Preemptive Strike 0.1 then it flips as he did nothing of what needed to be done. Do you have a band and need people to support it? Then you have to support events, specially in our island (Crete). He never went to a live, event, or party related to the genre. You cannot have it both ways, you need to interact with your fans face to face not behind a monitor from your living room.

<img src="/images/the_dread_files/jim.jpg" style="width:390;max-width:100%">


**What happened to your Facebook page? From what I understand it got hijacked in a way that I can only describe as pathetic.**

**Jim**: Things are simple about this. He originally created the page and decided that it belongs to him because of this hence keeping it for himself and his future projects. I do not judge his decision but if for any reason he wanted to "punish" me he could delete it from Facebook instead of having the laughable fact of having two "Preemptive Strike 0.1" pages. After 4 lives I could only create my own page for the band. Afterwards Facebook allowed us both to make small name changes so his page is now named "Teknovore" and mine "Preemptive Strike 0.1". Time will tell who will win and who will lose from it, but this was something really serious as I was out of the page and I had to start over from the beginning. It also contributed to not talk to each other ever since.


**Describe to us your latest release, "The Dread Files": music, lyrics, artwork.**

**Jim**: Yiannis Dseq wrote all the music and I compiled the lyrics. 3 out of 4 tracks are dedicated both from lyrics and corresponding samples to the 3 major works of Lucio Fulci, which are "Wormfaced Cadavers" for "Zombie Flesh Eaters", "Doctor Dread" for "House by the Cemetery", and "You will wake up in terror" to "The Beyond". "Evolution Retribution" has a sci-fi theme with samples taken from "The Children of the Damned". Artwork again is a work of Vlad Mc Neally and every quadrant of the cover depicts a corresponding track! I believe it is something that happens for the first time in this scene!

<img src="/images/the_dread_files/yiannis.jpg" style="width:390;max-width:100%">


**How is it different from the previous one?**

**Jim**: 100% different from "Progeny of the Technovore"! Do not think it has many trance influences as it is more based on old school dark electro patterns. A back to the roots approach I would say. Also lyrically as I mentioned before it is very different. As all fans of the band know: Preemptive Strike 0.1 never release the same record twice! 😀


**Yiannis Dseq**: Main difference with "Progeny..." in my opinion is that there is more atmosphere in the tracks with the harsh element being less present. BPMs are lower while aesthetically it is closer to dark electro more than the trance side of the scene. Although I believe there are still elements that refer to the recent PS01 style specially in "Revolution/Retribution".


**Where can someone find it?**

**Jim**: Either straight from DWA which released it in limited vinyl and MCD on the 25th of September 2020 and from everywhere else in Europe from where it is officially released after 11th of December 2020.


<img src="/images/the_dread_files/cover.jpg" style="width:390;max-width:100%">


**How has the composing/recording flow changed? How is it working only with Yiannis.**

**Jim**: Not many things changed, simply we have 100% non overlapping roles. Yiannis Dseq does all the music, synths, and programming while I do all related to drums, lyrics, vocals, samples without anyone stepping on other's toes. We discuss a lot about the structure of the tracks, something we did not have before... This which has changed a lot is the production and the mastering which for the first time is done from Yiannis Dseq with results making us feel that this is the right choice! About being only the two of us, I will repeat what has already been mentioned to previous interviews or statements that it is far better to work with someone with whom you are friends and have more or less the same approach versus someone with whom you have to collaborate together without anything in common apart from being in the same band.


**Yiannis Dseq**: This time I had more "space" to express my ideas. I generally like to write music and express myself based on how I feel at the time and what I listen to. Lately I listen to lots of dark electro and modern techno EBM. I would not like to repeat myself or do something which would look like a caricature of the established styles or sounds of the genre/scene. I would not even be able to do it.


**A few words for each band remixing you, starting with Llumen.**

**Jim**: We met all of them personally in Belgium when we played there and purposefully wanted to do this connect with remixers only from this country!

**PS01**: Amazing guy, we hang out on Belgium, was our sound engineer. His band although new has a special sound.

**Continuing with The Juggernauts...**

**PS01**: Rising Old school EBM band from one of the oldest members of the scene who facilitates most of the lives happening in this place. We did not expect them so easily to accept to remix us because our music differs a lot.


**Then Implant:**

**PS01**: There are not enough things to be told about Len Lemeire and this band. A unique - "magic" - figure in the genre with his remixes being memorable.


**Finally, IC 434:**

**PS01**: Fans of them since late 90s, major band, performed with them on Belgium this year, great person and artist, nothing else to say!

<img src="/images/the_dread_files/band_close.jpg" style="width:390;max-width:100%">


**For the last 2 years, which are your favourite releases from EBM/Industrial and Metal?**

**Jim**: Metal releases, although the last two years I listen to less metal: Raubtier: [Overlevare](https://www.discogs.com/Raubtier-%C3%96verlevare/release/14157314), Nachtblut: [Vanitas](https://www.discogs.com/Nachtblut-Vanitas/master/1826672), Anaal Nathrakh: [Endarkenment](https://www.discogs.com/Anaal-Nathrakh-Endarkenment/master/1813403).

EBM/Industrial: Numb: [Mortal Geometry](https://www.discogs.com/Numb-Mortal-Geometry/master/1594368), Synapsyche: [In Praise of Folly](https://www.discogs.com/Synapsyche-In-Praise-Of-Folly/master/1567093), Hocico: [Artificial Extinction](https://www.discogs.com/Hocico-Artificial-Extinction/master/1580883),  Suicide Commando: [Mindstrip Redux](https://www.discogs.com/Suicide-Commando-Mindstrip-Redux/master/1813526),  Placebo Effect: [Shattered Souls](https://www.discogs.com/Placebo-Effect-Shattered-Souls/release/16116940),  Atropine: [Human Emulsion ](https://www.discogs.com/Atropine-Human-Emulsion/master/1822831),  Armageddon Dildos: [Dystopia](https://www.discogs.com/Armageddon-Dildos-Dystopia/master/1821021),  Statiqbloom: [Asphyxia](https://www.discogs.com/Statiqbloom-Asphyxia/master/1560469),  Heimatærde: [Eigengrab](https://www.discogs.com/Heimat%C3%A6rde-Eigengrab/release/15230933), Technolorgy: [Inevitably Versatile](https://www.discogs.com/Technolorgy-Inevitably-Versatile/release/14307974).

**How the whole Covid thing has affected your band and how do you feel these days?**

**Jim**: Do not want to say a lot. It is the worst that has ever happened to me because it contradicts with my way of life. I cannot find anything positive in being quarantined, who ever says the contrary is at least lying.

**Yiannis Dseq**: Mood influences a lot my music writing. This quarantine isolation has a negative effect on how I feel. Definitely there was time available to write/create music. As everything in life this needs to be managed. Where it has definitely impacted the band negatively is the ability to play live.


**"Doctor Dread": is the base loop based on a classic electronic song? Feels eerily familiar, something like "Song for Denise" from " Piano Fantasia".**

**Yiannis Dseq**: No it is not based on a particular song. I have listened to lots of 80s music, so some of this might have an effect on the music I write.

**Anything else you want to say to your followers and to people reading this.**

**Jim**: Once the torturing we live is over we will see each other again either in lives or events, parties, etc. Then and only then we will write new material while we have the opportunity to present it live.

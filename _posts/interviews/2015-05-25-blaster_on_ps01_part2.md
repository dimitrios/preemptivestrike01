---
layout: posts
title:  'Blaster on Preemptive Strike - part 2'
date:   25-05-2015
categories: Interview
permalink: /interviews/blaster-on-preemptive-strike-part-2.html
---

Just after the release of "Epos-V", continuing from <a href="/interviews/blaster-on-preemptive-strike-part-1">part 1</a> with band's founding member Blaster.

**In the same lines we are recently witnessing the change of mentality from owning/buying the medium (such as CDs/Vinyls/cassette but also magazines/fanzines) to leasing/using it (streaming/YouTube and sites). What do you think about that? (I personally miss the printed material mostly while waiting for or inside a bus. It annoys me reading from the cell phone).**

Honestly I am not affected by what is happening nowadays. I am still buying my CDs and Vinyl. As for the printed material which I love, I will share a story: When side-line ceased to exist in its printed form in 2007, I started having withdrawal symptoms. So in 2008 I started studying German so that I would be able to read the only printed magazines that were available back then, with all of them still available today: <a target="_blank" href="http://www.sonic-seducer.de/">Sonic Seducer</a>", "<a target="_blank" href="http://www.zillo.de/">Zillo</a>", "<a target="_blank" href="http://www.orkus.de/">Orkus</a>" et. I got the degree two years later and since then I am a "Sonic Seducer" subscriber! In there you can read the "truth". Every other media is irrelevant, only what you can touch counts!

**Something that distinguishes the band is that its members live in Crete, something at the least... unusual. How is it not being a traditional musician in Crete? Does it affect your lives, apart from your musical influences? Does the press and people treat the band differently?**

It is indeed rare, although Crete had an industrial scene from 2006 until 2012 with parties and lives including foreign bands. Back in the 90s I was the only person involved on the island. I am afraid that this might be the case again since one after another people are jumping ship. Very few outside the scene know that I am a musician, something I don’t share with outsiders. So it has no effect on my personal life. The press (magazines, etc) from abroad get interested  when learning that there is an electro/industrial band from Crete and they treat us very positively.

**Jarrad Succumbs: One of the criticisms of dark electronic/industrial music is that the genre is stagnant and not evolving, and that musicians refuse to take risks for fear of being "not in the scene." Preemptive Strike 0.1 is one of the few bands that seem to break this mould, and freely experiments with different sounds, instruments, and other aspects of music but still keeping it dark. What drive is behind your willingness to experiment with different sounds in your music and do you have a goal beyond making music you enjoy yourselves?**

I definitely agree with you Jarrad. The scene has stagnated  especially since PCs and VST Plug came into our lives. Emotion and innovation perished. Also everybody refuses to take risks for fear of getting "off scene". We ourselves were not out of this, especially in our first albums. At some point I decided that enough is enough, not willing to perform this way again. We tried a little bit with "Kosmokrator" and then more with "T.A.L.O.S.". In the summer of 2013, when I started writing "Epos-V" I decided to break every rule - which I did: lyrically, including traditional instruments, sounds that had not been used before for this type of music as well as non-conventional melodies. I know that many will cast aside this album, but also that many will love it. If we ever make a new album there will be no "rule" or "form" of the genre standing. What will not change is the harshness of the sound and the vocals. We might become odd, but never decadent wimps! I will be very glad to be characterised as "outside the scene" mostly  by newcomers.

**Nexus Aspari: Who would you most like to collaborate with from the whole music industry?**

From the dark/electro industrial definitely with <a target="_blank" href="http://www.discogs.com/artist/20058-Feindflug">Feindflug</a> and maybe with the singer of <a target="_blank" href="http://www.discogs.com/artist/25404-Laibach">Laibach</a>, although I know he is a bit "arrogant". From metal bands, I always wanted to work with <a target="_blank" href="http://www.discogs.com/artist/253925-Summoning">Summoning</a>, which almost happened in 2009 but ... Also with <a target="_blank" href="http://www.discogs.com/artist/233588-Samael">Samael</a>, <a target="_blank" href="http://www.discogs.com/artist/256076-Amon-Amarth">Amon Amarth</a> and <a target="_blank" href="http://www.discogs.com/artist/11610-Fear-Factory">Fear Factory</a>.

<img src="/images/3572212.jpg" alt="Picture" style="width:401;max-width:100%">

**Nexus Aspari: Which period in history would you like visit and why?**

No period  that I haven't lived myself. I'd like to return in 1993-1999 and stay there forever!

**Nexus Aspari: What makes you happy/angry in life?**

There are people that love you and you love them &nbsp;back, especially if those &nbsp;support your choices. I have hobbies  such as body-building, football and above everything else buying CDs  and Vinyl. What makes me sad is the whole financial situation: I would  really like to have better earnings, as everybody... I also do not want my football team to lose, which is very difficult with the team I chose (<a target="_blank" href="http://http://en.wikipedia.org/wiki/Aris_F.C._%28Thessaloniki%29">Aris Thessalonikis</a><a style="" href="http://en.wikipedia.org/wiki/Aris_F.C._%28Thessaloniki%29">), which is not doing well...

**Nexus Aspari: What inspires you to write lyrics for your music?**

Nothing out of the ordinary here. What else except B-movie sci-fi films of 50s-60s. Honest and pure films, unlike today's Hollywood abominations that have the arrogance to be called science fiction films with their stupid digital effects. As for the lyrics I wrote for "T.A.L.O.S." and "Epos-V", they use Greek mythology as reference. I did that so that they can match with traditional instruments (<a target="_blank" href="http://en.wikipedia.org/wiki/Lyre">Lyre</a>&nbsp; and <a target="_blank" href="http://en.wikipedia.org/wiki/Bouzouki">Bouzouki</a>).

**Nexus Aspari: Which conspiracy theories if any, you think may be true?**

None whatsoever! These are for people that believe that others are responsible for their bad situation or the issues with their own existence or are trying to uncover a conspiracy behind every event so that they can look clever to their more-stupid-than-them audience.  "<em>Hey, we didn't get to the moon</em>", "<em>Jews brought down the twin towers</em>" or  "<em>The Government is spraying us with chemicals</em>". I honestly feel the need to punch every single person that says such crap.

---
layout: posts
title:  'Blaster on Preemptive Strike - part 1'
date:   08-04-2015
categories: Interview
permalink: /interviews/blaster-on-preemptive-strike-part-1.html
---

**When did you decide to write music?**

Since I was a kid, I got hooked on drums and tried to get into metal bands playing different genres. Later while listening to industrial for a decade I realized that being just a drummer made it impossible to run a band. My first attempt was in 2001, when with the help of a keyboard player I created "Extinction reprogrammed", which is something I say publicly for the first time. I made a demo with 4 tracks, paying loads of money to the studio, because neither of us knew how this music is produced. Maybe one day I will give our fans the opportunity to listen to it. It's something that reminds you of Cold Meat Industry releases. I'd say death/industrial without using a computer. Everything is analogue! The rest is as told by Cryon, so there is no need to repeat it!  

**Your top five influences. Why?**

From the industrial genre: <a title="" target="_blank" href="http://www.discogs.com/artist/20058-Feindflug">Feindflug</a>, <a title="" target="_blank" href="http://www.discogs.com/artist/3180330-Funkervogt">Funker Vogt</a>, <a title="" target="_blank" href="http://www.discogs.com/artist/93779-E-Craft">E-Craft</a>, <a title="" target="_blank" href="http://www.discogs.com/artist/15574-Decoded-Feedback">Decoded Feedback</a> and <a title="" target="_blank" href="http://www.discogs.com/artist/8943-Numb">Numb</a>. All five have a personal style that cannot be imitated. I'd put Front Line Assembly first , but their releases fluctuate in quality being either masterpieces or commercial crap.

**Give us a top-5 of your records (or top-5 metal, top-5 industrial if you cannot make up your mind).**

From the industrial genre:

1.  <a title="" target="_blank" href="http://www.discogs.com/artist/10656-Front-Line-Assembly">Front Line Assembly</a> - "Millenium": The absolute record, which made me an industrial fan, while I cannot yet get the fact that such a release was made by human beings specially in 1993-4!
2.  <a title="" target="_blank" href="http://www.discogs.com/artist/8943-Numb">Numb</a>- "Death on instalment plan": When I heard it, it felt like there could not be any better and darker/harsher music.
3.  <a title="" style="" target="_blank" href="http://www.discogs.com/artist/15574-Decoded-Feedback">Decoded feedback</a> - "Technophoby": The absolute darkness a-la pure electro
4.  <a title="" style="" target="_blank" href="http://www.discogs.com/artist/3180330-Funkervogt">Funker Vogt</a> - "Execution tracks": The title says it all, no more comments...
5.  <a title="" style="" target="_blank" href="http://www.discogs.com/artist/15020-Suicide-Commando">Suicide Commando</a> - "Mindstrip": No one could even get closer to this, not even themselves...

From the metal genre, without any particular order or any comments, I give you:

-   <a target="_blank" href="http://www.discogs.com/artist/233588-Samael"> Samael</a> - "Solar Soul"
-   <a title="" target="_blank" href="http://www.discogs.com/artist/11771-Rammstein">Rammstein</a> - "Mutter"
-   <a title="" target="_blank" href="http://www.discogs.com/artist/261002-Septic-Flesh">Septic Flesh</a> - "Titan"
-   <a title="" target="_blank" href="http://www.discogs.com/artist/1547766-Ex-Deo">Ex-Deo</a> - "Kaligula"
-   <a title="" target="_blank" href="http://www.discogs.com/artist/272324-Ensiferum">Ensiferum</a> - "One man army"

**A top 10 of science fiction movies for a PS01 fan.**

Definitely many 50s-60s B-Movies! "<a target="_blank" href="http://www.imdb.com/title/tt0046534/">War of the worlds</a>", "<a target="_blank" href="http://www.imdb.com/title/tt0049169/">Earth VS the flying saucers</a>", "<a target="_blank" href="http://www.imdb.com/title/tt0052564/">The angry red planet</a>", "<a target="_blank" href="http://www.imdb.com/title/tt0050539/">The incredible shrinking man</a>", "<a target="_blank" href="http://www.imdb.com/title/tt0049366/">Invasion of the body snatchers</a>", but also more recent ones such as: "<a target="_blank" href="http://www.imdb.com/title/tt0084787/">The thing</a>", "<a target="_blank" href="http://www.imdb.com/title/tt0078748/">Alien</a>", "<a target="_blank" href="http://www.imdb.com/title/tt0120201/">Starship troopers</a>", "<a target="_blank" href="http://www.imdb.com/title/tt0091064/">The fly</a>", "<a target="_blank" href="http://www.imdb.com/title/tt0116629/">Independence day</a>", "<a target="_blank" href="http://www.imdb.com/title/tt0482571/">Τhe prestige</a>".

**You have already been asked many times, allow once again: There was a change of musical direction early on. Was it related to influences? Something else?**

We were simply not doing what we really wanted to, so within the first year we flipped to the the genre that we always wanted to be in. Of course after 12 years with "Pierce their husk MCD/7''" we got back to guitar based style, but armed with more experience.

**A member of your band wrote at some point that whoever played "Subuteo" as a kid will have some experiences that PES or Fifa can never offer. Extending this,  what is the current generation of musicians missing?**

It's true, I wrote it. Even if I am playing PES, what I have experienced with Subuteo cannot be easily described. I'd of course like to add that in local matches, it took me 8 years to lose a gaming match. I have to admit that I suck at PES. What I had as a musician was the feeling or hardware: Synth/Sequencer/Sampler. Also being drawn among a mess of cables while trying to get the max out of my equipment. I still use my mobile sampler to extract samples from movies. Although it might sound arrogant, I don't believe that there is anyone in the scene that I'd admit they  can utilize movie samples better than me...  

**Also the other way around, what was available "back then" available that you wished you had today?**

I'd say everything. I grew up in the late 80s, but I don't reminisce them. The 90s where better specially as far as dark electro music is concerned. If you wanted to get something, you had to try hard ,  while the feeling of discovering a rare CD was unique! You had to struggle in order to learn things, not everything was available. I find nothing wrong with the 90s, no matter what some newcomers might say.


<img src="/images/1272795.jpeg" style="width:397;max-width:100%">

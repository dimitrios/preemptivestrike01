---
layout: posts
title:  'Chaosvault Interview'
date:   05-01-2015
categories: Interview
permalink: /interviews/chaosvault-interview.html
---

 ![The Band](/images/8724398.jpg)

"Dziś taka trochę ciekawostka, bowiem grecki PreEmptive Strike O.1 nie jest zespołem stricte metalowym, choć z metalem ma sporo wspólnego.

Chcieliśmy jednak zaprezentować czytelnikom Chaos Vault ten zespół, bo jest dość ciekawym punktem na muzycznej mapie Europy, a niektóre ich materiały mogą przypaść do gustu osobom poszukującym satysfakcji również w nieco innych dźwiękach. Jakich? Przeczytajcie wywiad. ..."

More:[http://chaosvault.com/wywiady/preemptive-strike-0-1-uwazam-ze-jestesmy-jedynym-zespolem-na-scenie-electro-ktory-potrafi-grac-w-dwoch-tak-roznych-stylach-naraz/](http://chaosvault.com/wywiady/preemptive-strike-0-1-uwazam-ze-jestesmy-jedynym-zespolem-na-scenie-electro-ktory-potrafi-grac-w-dwoch-tak-roznych-stylach-naraz/)

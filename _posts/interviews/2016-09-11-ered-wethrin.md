---
layout: posts
title:  'Mini interview from Sven/Ered Wethrin about the collaboration on "Eternal Masters"'
date:   11-09-2016
categories: Interview
permalink: /interviews/mini-interview-from-svenered-wethrin-about-the-collaboration-on-eternal-masters.html
---
<img src="/images/logos/ered.jpg"/><br>

**Hi Sven, describe your band to someone who just read your name next to a PS01 track, knowing nothing about you.**

Ered Wethrin is an epic and atmospheric Black Metal project that in the vein of 90s Black Metal. EW uses guitar riffs to drive the atmosphere and implements keyboard elements as an accompaniment.

**How did you get to know PS01, what led to your collaboration? Why did you chose to work on ETERNAL MASTERS?**

PS01 contacted me about collaboration and, after hearing their music, I decided to work with them.

**Which is your opinion about crossovers between bands of different genres? Have you tried it in the past, will you try again in the future?**

Sometimes it works and sometimes it doesn't. If you have a knack for hearing the elements blended together and aren't just forcing it in the name of being novel, then the potential for creating something great is there. Also, both of the genres being blended have to be somewhat enjoyed by the listener, or those incorporated elements aren't likely to be pleasing to them.

**Where can someone learn more about you and your work?**

They can check out the videos on <a target="_blank" href="https://www.youtube.com/watch?v=HqypaIK8mbs&amp;list=PLzAg74O-lDukJOHP1wSJemwzp9qiQrvIu">YouTube</a> or link to the <a target="_blank" href="https://www.facebook.com/eredwethrinofficial/">Facebook page</a>&nbsp; (Ered Wethrin/black metal) or find out more at the label's online store (<a target="_blank" href="http://shop.northern-silence.de/">Northern Silence Productions</a>).

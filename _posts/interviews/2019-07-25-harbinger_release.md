---
layout: posts
title: "Harbinger post Release Interview"
date: 25-07-2019
categories: Interview
permalink: /interviews/harbinger.html
---

<img src="/images/covers/harbinger.jpg" style="width:390;max-width:100%">

Follow up from the release of "[Harbinger](https://www.discogs.com/PreEmptive-Strike-01-Harbinger/master/1572348)"
from DWA, which you can purchase <a href="https://preemptivestrike01.bandcamp.com/album/harbinger" target="_blank">here</a>. Cryon answering questions on behalf of the band.

**What has happened in the PS01 camp in the last three years?**

It’s been business as usual. Even before "[Through the Astral Cold](https://www.discogs.com/PreEmptive-Strike-01-Through-The-Astral-Cold/master/1128196)" came out I was already working on the next release. Since then we’ve released a single for the track "[More Than Flesh](https://www.discogs.com/PreEmptive-Strike-01-More-Than-Flesh/master/1167177)", an updated version of the "[Eternal Masters](https://www.discogs.com/PreEmptive-Strike-01-Eternal-Masters-2-Anthropophagic-Retaliation/master/1552824)" EP and, most recently, the "Harbinger" EP which features 3 previously unreleased tracks and serves as a precursor for our new album which we’ve just completed today in fact - So a "harbinger" in more ways than one.

<img src="/images/band-harbinger/ps01-harbinger-001.jpg" style="width:390;max-width:100%">

**How and why has your sound changed in "[Through the Astral Cold](https://www.discogs.com/PreEmptive-Strike-01-Through-The-Astral-Cold/master/1128196)"?**

Obviously "[Through the Astral Cold](https://www.discogs.com/PreEmptive-Strike-01-Through-The-Astral-Cold/master/1128196)" is very different from our two previous albums – "[T.A.L.O.S.](https://www.discogs.com/PreEmptive-Strike-01-TALOS/master/441038)" and "[Epos V](https://www.discogs.com/PreEmptive-Strike-01-Epos-V/master/837208)". It was time to move on from that sound for me. It had completed its cycle and I didn’t feel like repeating myself.

With TTAC I started toying with the idea of incorporating Goa Trance as an influence. I find the history of Goa Trance and how it’s connected to EBM quite interesting. It basically evolved from EBM, New Beat and other genres that arrived on tape cassettes by traveller-collectors and DJs and were shared tape-to-tape among Goa DJs, in an underground scene not driven by labels or the music industry. I actually find it strange that it hasn’t found its way back to EBM as an influence with very few exceptions – [This Morn’ Omina](https://www.discogs.com/artist/94063-This-Morn-Omina) and [Nebula-H](https://www.discogs.com/artist/131849-Nebula-H) are the only ones that come to mind. Instead harsh EBM was influenced by Psytrance and Eurotrance/Eurodance but I aim to change that..

**What is the deal with "Harbinger", is it a limited release?**

Essentially Harbinger is the second single in support of TTAC after "[More Than Flesh](https://www.discogs.com/PreEmptive-Strike-01-More-Than-Flesh/master/1167177)". However it features three unreleased tracks as I mentioned above and is accompanied by a [music video](https://www.youtube.com/watch?v=HDITSlRrViw) for the title track. It has a limited run on clear and black vinyl as well as on CD – in fact, at the time of writing the clear vinyl is sold out and there are only 4 copy of the black vinyl left. So if anyone wants to grab those, act fast!

<img src="/images/band-harbinger/ps01-harbinger-002.jpg" style="width:390;max-width:100%">

**How was working with Vlad McNeally (and where can we find more of his work)?**

We have a very good rapport with Vlad and every cover he’s done for us has been awesome. He’s good at translating my vague instructions into the great covers you see! I think my favourite cover he’s designed for us is Harbinger – so good. You can find more of his work on his site <http://kallistidesign.net/> and his <a href="https://www.discogs.com/artist/2184774-Vlad-McNeally">Discogs page</a>. Definitely check it out.

**How was working with Dimitris N. Douvras? (think it is not the first time)**

We’ve worked with Dimitris since our very first album, before even. He is without doubt the best producer for electronic music in Greece. We’ve worked together for so long that he kind of knows what we want without too much direction. I’m certain we will continue working with him for as long as PES0.1 exists.

<img src="/images/band-harbinger/ps01-harbinger-003.jpg" style="width:390;max-width:100%">

**You started doing lives. How many so far? Bands tour less as time passes, you started touring more, what is going on?**

Calling it touring is kind of a stretch to be honest! We had a cluster of live shows in late 2018 and that’s about it. It was lots of fun and hopefully we can repeat it sooner rather than later. Fingers crossed that the new album blows up and we can get some more opportunities to play abroad again.

**Let’s discuss streaming, people have started becoming disappointed. Which is your experience,your thoughts?**

The music industry as a whole is in flux at the moment. Everyone is still trying to figure out how the new reality of streaming and Internet culture is going to shape business models. Streaming is extremely difficult to quantify and frankly Spotify feels like more like an industry-sanctioned scam against the artists than anything else.

For small scenes such as our own it is almost essential that people continue to actually buy albums, if not physically than at least digitally. Thankfully that seems to be the case judging by how people respond to our vinyl releases, which in actuality have a small margin for profit but prove fans still appreciate a special release so to speak.

<img src="/images/band-harbinger/ps01-harbinger-004.jpg" style="width:390;max-width:100%">

**How has the scene evolved in the last 3 years?**

A few scene bands have managed to gain more mainstream appeal in recent years which is always great as it means more exposure for the scene in general.

There’s a lot of uncertainty right now which relates to the issues I mentioned in the previous question. It’s kind of a ‘time will tell’ situation as there is mostly speculation about what comes next at the moment.

**How is your new label? Seems like the "to-go" place for bands such as yours? That from seeing stuff like vinyl, limited editions, selling on multiple channels.**

[DWA](http://www.dwa.rocks/) is the right fit for us. A very active and passionate label which takes personal pride in each and every one of its releases and always trying to think outside the box to make each release special in terms of format or promotion. You can’t ask for much more! Jamie Nova (DWA’s boss) had been a personal friend of the band for a few years before we joined so it was a natural choice.

<img src="/images/band-harbinger/ps01-harbinger-005.jpg" style="width:390;max-width:100%">

**Next immediate steps for the band.**

Right now…just waiting for a release date for the new album. I wish I could reveal more than just “it’s done” but that’s really all I can say!

**Links**

- [Video for Harbinger on YouTube](https://www.youtube.com/watch?v=HDITSlRrViw)
- [Harbinger at BandCamp](https://preemptivestrike01.bandcamp.com/album/harbinge)

---
layout: posts
title:  'Jim the Blaster @ Industrial Zone'
date:   01-11-2024
categories: Interview
permalink: /interviews/biomixaniki-zoni-interview.html
---

Industrial Zone is a radioshow for industrial/ebm/synth/dark/avant-garde etc music. In this episode, host Yannis Kamarinos has a brief discussion with Jim the Blaster, main member of the well-kwon Greek electro-industrial band Pre-Emptive Strike 0.1.

<iframe width="100%" height="300" scrolling="no" frameborder="no" allow="autoplay" src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/1941271791&color=%231a2c4a&auto_play=true&hide_related=false&show_comments=true&show_user=true&show_reposts=false&show_teaser=true&visual=true"></iframe><div style="font-size: 10px; color: #cccccc;line-break: anywhere;word-break: normal;overflow: hidden;white-space: nowrap;text-overflow: ellipsis; font-family: Interstate,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Garuda,Verdana,Tahoma,sans-serif;font-weight: 100;"><a href="https://soundcloud.com/user-413806992-348261109" title="Yannis Kamarinos" target="_blank" style="color: #cccccc; text-decoration: none;">Yannis Kamarinos</a> · <a href="https://soundcloud.com/user-413806992-348261109/industrial_zone_jim_the_blaste" title="INDUSTRIAL_ZONE_JIM_THE_BLASTER" target="_blank" style="color: #cccccc; text-decoration: none;">INDUSTRIAL_ZONE_JIM_THE_BLASTER</a></div>

Source: <https://soundcloud.com/user-413806992-348261109/industrial_zone_jim_the_blaste>

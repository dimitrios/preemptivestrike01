---
layout: posts
title:  'Brutal Resonance interview'
date:   29-10-2014
categories: Interview
permalink: /interviews/brutal-resonance-interview.html
---
![The Band](/images/7582847_orig.jpg)

With having their most recent metal inspired release [Pierce Their Husk](http://brutalresonance.com/review/preemptive-strike-01-pierce-their-husk/) reviewed, I sort of had burning desire to know which route&nbsp;[PreEmptive Strike 0.1](http://brutalresonance.com/artist/preemptive-strike-01/) would be taking next.

Already a fan of the well received [T.A.L.O.S.](http://brutalresonance.com/review/preemptive-strike-01-talos/), I wanted to know more about which direction this Greek act would be trailing next. And, I did just that in the interview below. Read on to find out more about Preemptive's future plans: [http://brutalresonance.com/interview/preemptive-strike-01-interview-sep-2014/](http://brutalresonance.com/interview/preemptive-strike-01-interview-sep-2014/)

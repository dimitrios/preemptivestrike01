---
layout: posts
title:  'Yiannis DSeq on Preemptive Strike!'
date:   11-03-2015
categories: Interview
permalink: /interviews/yiannis-dseq-on-preemptive-strike.html
---

Due to the nature of the Internet, most interviews are gravitating around the latest releases of each band and/or an upcoming tour. Being the official site of the band and having a fanzine attitude, we thought it would be good to ask some questions about the history of the band or the band in general, that would interest a fan in any given time.

A list was compiled and given to each member of the band separately. These are the answers from Yiannis DSeq, band's newest member.

**Which was the first record you bought?**

I had bought a number of CDs, there is no "first album" for me :). In theory from the dark electro sphere, it must have been Suicide Commando's "<a href="http://www.discogs.com/Suicide-Commando-Axis-Of-Evil/master/13269" target="_blank">Axis of evil</a>".

**When did you decide to write music?**

I wanted to write music since I was 15. As a fanatic metal-head I had started the first guitar lessons and had the first attempts.

**Which are your top ten influences, and why?**

After listening to music for a number of years, it has become difficult for me to track down my influences. I'm a fan of extreme and technical metalas well as dark electro and some trance. Bands that have influenced me but without being necessary my favorite ones are:

-   "<a title="" target="_blank" href="http://www.discogs.com/artist/15574-Decoded-Feedback">Decoded feedback</a>" (dark, complete compositions),
-   "<a title="" target="_blank" href="http://www.discogs.com/artist/79541-Aslan-Faction">Aslan faction</a>" (old school and dark),
-   "<a target="_blank" href="http://www.discogs.com/artist/227814-SITD">STID</a>" (flawless on their sound, wonderful melodies and compositions),
-   "<a href="http://www.discogs.com/artist/69226-Hocico" target="_blank">Hocico"</a> (a whole chapter in music on their own; although stretched too far and saturated, they have created their own school of music).
-   Many people say to me that I have been influenced from the first record of "<a target="_blank" href="http://www.discogs.com/artist/115210-Aesthetic-Perfection">Aesthetic Perfection</a>", who is my favorite one from them,but I was very disappointed from the band after their third album both musically as well as their image. In the last years I tend to come back a lot to 90s.

**Give us a top-5 of your records (or top-5 metal, top-5 industrial if you cannot make up your mind).**

-   <a href="http://www.discogs.com/artist/200592-Fractured" target="_blank">Fractured</a> - "Only human remains". Had a big shock when I listened to it, technical, harsh, melodic, something that will never be repeated again.
-   <a href="http://www.discogs.com/artist/15574-Decoded-Feedback" target="_blank">Decoded feedback</a> - "Shockwave". Dark powerful and aggressive. Dark industrial, although maybe not their darkest attempt, it is harsh and somehow more minimal in comparison with "Biovital". They have many musical influences and distance themselves from musical stereotypes.
-   <a target="_blank" href="http://www.discogs.com/artist/69226-Hocico">Hocico</a> - "Signos de aberracion". A record full of hits while at the same time the atmosphere and aggressiveness of Hocico is more than present. Many effects, trance and future pop influences, but the Hocico "sickness" and "hate" is always present; always there.
-   <a target="_blank" href="http://www.discogs.com/artist/121722-Tactical-Sekt">Tactical Sekt</a> - "Syncope".  Musically flawless from every aspect in music, sound and lyrics. Dance oriented tracks but from a "serious" band. Awesome vocals combined with trancy and aggressive music that has a hidden old school dark electro aspect.
-   <a target="_blank" href="http://www.discogs.com/artist/119015-Object-2">Object</a> - "Mechanisms Of Faith". Experimental and slow dark electro  with it's own identity. FX, atmosphere, melodies in a huge variety. Original electronic music that distances itself from the commercial garbage which have filled the scene.
-   <a target="_blank" href="http://www.discogs.com/artist/10734-Assemblage-23">Assemblage 23</a> - "The vinyl sessions". Maybe shouldn't be in the list, since it is a collection. But it has awesome... vinyl tracks from my favorite band in it's genre. The best mix of synth-pop and modern EBM.

**Again top 10 science fiction movies.**

I am not really a big movie fan...

**You have already been asked many times, allow once again: There was a change of musical direction early on. Was it related to influences? Something else?**

I am a recent member to the band, so cannot provide a good answer.

 <div><div class="wsite-image wsite-image-border-thin " style="padding-top:10px;padding-bottom:10px;margin-left:0;margin-right:0;text-align:center"> <a> <img src="/images/9274495.jpg" style="width:379;max-width:100%"> </a> <div style="display:block;font-size:90%"></div></div></div>

**A member of your band wrote at some point that whoever played "Subuteo" as a kid will has some experiences that PES or Fifa can never reproduce. Extending this what is the current generation of musicians missing?**

Maybe people back then were more emotionally involved, something that had to so with the fact that everything was more difficult. Because of that if you wanted to get into the process of discovering music or make music, you had to invest a big part of your time and that created a different dynamic of emotional involvement. Now everything is more available or someone could argue "easy".

Saying though that one period was better or worse than another is something very subjective.

**Also the other way around, what was "back then" available that you wished you had today?**

Cassettes! :P At least not that much available as they used to be...

**In the same lines we are recently witnessing the change of mentality from owning/buying the medium (such as CDs/Vinyls/cassette but also magazines/fanzines) to leasing/using it (streaming/YouTube and sites). What do you think about it? (I personally miss the printed material mostly while waiting for or inside a bus. It annoys me reading from the cell phone.**

I am generally positive since the access to more information is easier. Now how it is being used from each individual is a completely different story. Maybe some people miss the printing press, but it's function/essence is still here.

**Something that distinguishes the band is that it's members live in Crete, something  uncommon. How is it not being a traditional musician in Crete. Does it affect your lives, apart from your musical influences? Does the press and people treat the band differently?**

Crete is not only about dancing and the Crete-Lyra that some people might be thinking, probably because of the tourist connotation. It is though Greek countryside/province as many other places in the country. The band is not so popular because people have different tastes and mentality. This makes our existence a little bit more discreet, because of the surrounding status quo. So we live our regular lives with music as something more personal...

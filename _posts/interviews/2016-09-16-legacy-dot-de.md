---
layout: posts
title:  "Interview with legacy.de"
date:   16-09-2016
categories: Interview
permalink: /interviews/interview-on-legacyde.html
---

![Logo](/images/logos/legacy-logo_orig.png)

Interview with Jonathan Jancsary:

"Jim und George haben Euch schon im Print-Teil unseres Interviews einiges über ihre neue MCD „Eternal Masters“ und die Kollaboration mit verschiedensten Acts aus dem Black/Death Metal-Universum erzählt. Im Online-Teil soll das noch vertieft werden, denn schließlich gibt es noch so viel zu erzählen. ..."

More: [http://www.legacy.de/component/k2/item/34060-preemptive-strike-0-1](http://www.legacy.de/component/k2/item/34060-preemptive-strike-0-1)

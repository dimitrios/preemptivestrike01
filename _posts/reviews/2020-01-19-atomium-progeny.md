---
layout: posts
title:	"Review of Progeny on Atomium magazine"
date:	13-01-2020
categories: Review
permalink: /reviews/atomium_progeny.html
---

# Review of "Proyegeny of the Technovore" on Atomium magazine

In Spanish:

> El 25 de noviembre del 2019 la banda griega PREEMPTIVE STRIKE 0.1, lanzó su nuevo álbum de estudio llamado "Proyegeny of the Technovore". Es el álbum hasta ahora más ambicioso de la banda, ya que incluye elementos del EBM, Goa Trance y Aggrotech, sonidos con los que lograron darle un toque más futurista e implacable a sus nuevas canciones.

> El primer track del álbum "Progeny of the Technovore", parte de inmediato con ritmos rápidos y potentes, acompañado de una voz agresiva y a veces robótica, siendo un claro ejemplo de este giro que la banda le dio a su nueva producción.

> ...

More: <https://www.facebook.com/atomiummagazine/photos/a.1759425767608064/2477750222442278/?type=3&theater>


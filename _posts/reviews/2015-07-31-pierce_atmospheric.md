---
layout: posts
title:  '"Pierce..." on Atmospheric'
date:   31-07-2015
categories: Review
permalink: /reviews/pierce-on-atmospheric-polish.html
---
<img src="/images/logos/4932842_orig.png" style="margin-top: 5px; margin-bottom: 10px; margin-left: 0px; margin-right: 10px; border-width:1px;padding:3px; max-width:100%">

> "Garść faktów. PREEMPTIVE STRIKE 0.1 to grecki zespół elektro-metalowy. Nieszczególnie śledzę tego rodzaju muzykę, więc umknął mi fakt, że grają od 2003 roku i mają już na koncie cztery albumy, nie licząc pomniejszych wydawnictw. „Pierce Their Husk” to najnowsza EPka zespołu, reklamowana gościnnym udziałem Niklasa Kvarfortha (SHINING) i zawierająca, jak to bywa przy okazji tego typu wydawnictw, remiksy. ... "

Review by: Michał Pawełczyk.

More: <http://atmospheric.pl/preemptive-strike-0-1-pierce-their-husk-14/>

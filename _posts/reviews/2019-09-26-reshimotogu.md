---
layout: posts
title:  "Anthropomorphic Retaliation on reshimotohu"
date:   23-07-2019
categories: Review
permalink: /reviews/reshimotohu.html
---

# Review of "Anthropomorphic Retaliation"

## Translation

Well, this one has been lying on our desk for a while.
Somehow, I've fallen asleep on my guard, not giving it its deserved attention.
In case you've been wondering, it's another industrial extreme metal release by our friends from PES 0.1.

I got it as a gift from their frontman Jim, quite a while ago. Now I regret being so oblivious to the extent of goodness that’s been hiding in this humble slip-case of a release for such a long time…

So, what can I say? Given his work with Punishment Systems, one should only anticipate pure industrial greatness inside. And that’s exactly what’s delivered here.

The same dystopian atmosphere from Punishment Systems is there, alive and in the flesh. Wailing sirens, foreboding public announcements, the rattling sound of rusted machinery… It’s all there in the sound of industrial metal par-excellence: fast, cold, relentless and oh so wonderfully impersonal and mechanistic. The new inferno is a nightmare set in a future beyond our wildest dreams.

And they’ve finally got the chance to fulfill an old dream and collaborate with Red Harvest! Congrats you guys, the result of your collaboration has blown my mind. You deserved every bit of it!

The version of Antropophagus with Black Altar does great service to the song by arming it with an extra terrifying kick, making it fully realize its potential. Now that’s the sound of being ripped apart by cannibal\machine hybrids.

This is much more than an “extension pack” to Eternal Masters. In fact, it’s much more powerful than the main release which it aims to complement… Being a mere 20 minutes of hard-hitting aggression, much of its power might as well come from its condensed nature.

They've got a new release now. It sounds drastically different from this one, but in a rather intriguing way. Must admit I’m more into the aggressive side of PES, but there’s no doubt that their new release is very well made.

Speaking about these guys, apparently, they’re willing to venture a risky escape from the scary alien prison they’ve got themselves in (https://youtu.be/HDITSlRrViw), just to perform here in Israel.

Anyone here up to the task of hosting a bunch of interstellar refugees for a gig?

## Original text
העתיד הוורוד התקלף מזמן. זה זמן מלחמה. | מאת: נועה-קושניר ארצי

טוב, זה דרש את התייחסותי כבר הרבה מאוד זמן.

איכשהו נרדמתי על המשמר, והתוצאה הייתה שהדבר הזה לא זכה לתשומת הלב הראויה.

למקרה שתהיתם, מדובר בהוצאת אקסטרים מטאל אינדסטריאלי נוספת מידידינו PreEmptive Strike 0.1.
ה-MCD הזה התגלגל לתיבת הדואר שלי כמתנה מסולן הלהקה Jim Theblaster. עדיין לא ברור לי איך הצלחתי להתעלם מהפצצה ששכבה אצלי במשך תקופה כל כך ארוכה במעטפת הקרטון הפשוטה.

ומה אוכל לומר? בהתחשב בעבודתו של Jim עם Punishment Systems², נותר רק לצפות לאינדסטריאל מטאל מהזן המשובח ביותר. וזה בדיוק מה שתמצאו בפנים.

אותה אווירה דיסטופית שאפפה את Punishment Systems מומחזת לה כאן מחדש. צופרי האזעקה המייללים, קול מערכות הכריזה מבשרות הרעות, שקשוקי המכונות החלודות... כל אלו משתלבים לכדי יצירות אינדסטריאל מטאל מופתית: מהירה, אכזרית ונטולת כל רמז לאנושיות. הגיהינום המודרני הוא חזון בלהות עתידני העולה על כל דמיון.

והם סוף כל סוף זכו להגשים חלום ישן בשיתוף הפעולה המוצלח שלהם עם Red Harvest Official. (ברכותיי חברים, יצא מקסים). והגרסה ל-Antropophagus עם Black Altar... זו ממש הצליחה להטעין את השיר באימה טהורה הראויה יותר כפסקול להשתוללות צמאת הדם המתחוללת במילותיו.

ההוצאה הזו היא הרבה יותר מנספח קיקיוני ל-Eternal Masters שיצא לפני כשנתיים. למעשה, היא הרבה יותר עוצמתית מההוצאה שאותה היא באה להשלים. האם זהו הניסיון שנצבר בפרק הזמן שעבר בין שתי ההוצאות, שמדבר? האם זהו אורכה, שאינו עולה על 20 דקות, המונע מאוזנינו להסתגל לאגרסיה הטהורה המוטחת באפרכסותיה?

יש להם אלבום חדש עכשיו. הוא שונה באופן משמעותי מההוצאה הזו, ובאופן די לא צפוי עבור להקה המייצגת את פניה האגרסיביים יותר של ה-EBM. אני חייבת להודות שאני מתחברת יותר לפן הכבד של PES, אבל אין ספק שמדובר בהוצאה עשויה היטב.

ואם כבר מדברים על החבר'ה האלה, מסתבר שהם מוכנים להסתכן בניסיון הימלטות מהכלא החייזרי המפחיד הזה (https://youtu.be/HDITSlRrViw), רק כדי להופיע בפני הקהל הישראלי.

אז מי מרים את הכפפה ומביא את חבורת פליטי החלל הזו להופעה בארץ הקודש?
(קישור בנדקאמפ בתגובה ה-1).




---
layout: posts
title:  '"Epos-v" in medienkonverter.de'
date:   04-07-2015
categories: Review
permalink: /reviews/epos-v-in-medienkonverterde.html
---
<img src="/images/reviews/6341522.jpg" style="margin-top: 5px; margin-bottom: 10px; margin-left: 0px; margin-right: 10px; border-width:1px;padding:3px; max-width:100%">

> "Bisher kannte ich Preemptive Strike 0.1 nur von Samplerbeiträgen, die in mir nicht wirklich den Drang nach einen "mehr" weckten. Wie gut, dass es den Medienkonverter gibt und 'Epos V' bei mir gelandet ist. Denn meine Meinung hat sich zwar nicht um 180°, wohl aber immerhin um 120° gewendet und das Duo von der schönen aber derzeit arg von kapitalistischen Nöten geplagten Insel Kreta hat sich in meinem Schädel recht locker vor Heimataerde geschoben, wenn es um Hellectro meets traditionelle Klänge geht. Durch Recherche weiß ich nun, dass dies spätestens auf dem Vorgänger 'T.A.L.O.S.' wichtiges Elemtent war und die Band auf dazwischenliegenden Kleinveröffentlichungen eher dem Metal zugewandt war, allein der fundierte Vergleich fehlt mir und so bearbeite ich 'Epos V' mit jungfräuchlicher Unbelastetheit. ..."

Rank: 4.5 / 6

More: <http://www.medienkonverter.de/reviews-preemptive-strike-0.1_epos-v-5761.html>

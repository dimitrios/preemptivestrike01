---
layout: posts
title:  '"Epos V" on Sonic Seducer'
date:   14-06-2015
categories: Review
permalink: /reviews/epos-v-on-sonic-seducer.html
---
<img src="/images/reviews/8402559.jpg?467" alt="Picture" style="width:467;max-width:100%">

A translation with the help of Torsten Lißner:

The Greeks are crazy!

Writing the Argonauts saga with (the normally limited) resources of dark electronic music, using instruments like Lyra or Bouzouki, combine it with black metal influences and using some good Italodisco sounds, it sounds totally crazy for the normal Dark Electro fan.

You guys starting "Epos V" with a Sabaton Cover, which sounding like the best moments of a Dance or die song (Time Zero)
PS01 have the courage for doing such a crazy sound mixing. If someone things this album will flop, because of the craziness of this album, he things wrong because on "Epos V" PS01 will win more than lose like other bands who are trying such experiments (like the Sabaton Cover)

You sound way better than the normal electro bands and you have a higher quality level than them. Now the only thing you have to do, is to hope that the "normal" Dark Electro listener will try this mix of yours and like it. Your album has a great variety in comparison to other albums in this genre. And maybe you make your own.

genre: Epic dark electro

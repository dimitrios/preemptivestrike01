---
layout: posts
title:  '"Eternal Masters" on Brutal Resonance'
date:   07-09-2016
categories: Review
permalink: /reviews/eternal-masters-on-brutal-resonance.html
---
<img src="/images/reviews/ps01-eternal-masters_orig.jpg" style="width:auto;max-width:100%">

> "Greek outfit Preemptive Strike 0.1 has dominated my personal playlists over the years with substantial releases such as T.A.L.O.S. and their latest album EPOS V. They were both stellar albums that took the best stories from Ancient Greek Mythology and translated them into harsh EBM, dance-floor bombs. That being said, in between those two albums came the very metal riddled Pierce Their Husk single released via Odium Records. On it, the song that originally appeared via T.A.L.O.S. was given new life alongside other Preemptive Strike 0.1 tracks. The single also featured Niklas Kvarforth of Shining. Now, I'm sure I wasn't the only one who thought this was foreshadowing what was to come in the future, and now with their signing to DWA it appears as if Preemptive Strike 0.1 has realized their next true calling. ..."

Review by Steven Gullotta,

More: <http://brutalresonance.com/review/preemptive-strike-01-eternal-masters/>

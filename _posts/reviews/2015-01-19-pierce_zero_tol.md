---
layout: posts
title:  '"Pierce..." on Zero Tolerance'
date:   19-01-2015
categories: Review
permalink: /reviews/pierce-on-zero-tolerance.html
---
<img src="/images/reviews/5244992.jpg" style="margin-top: 5px; margin-bottom: 10px; margin-left: 0px; margin-right: 10px; border-width:1px;padding:3px; max-width:100%">

> "This dark electro EP/mini album has BM allegiances (guest appearances by Kvarforth and ex-Belphegor drummer Nefastus), but surprises by being somewhat dance-oriented rather than grinding and industrial. The title track appears in two forms, both featuring an unusually restrained but effective Kvarforth vocal. The original mix is surprisingly clean and bouncy, while the Skiltron NV 101 remix is almost thrashy, the synth bass riff instead being played by guitars, giving the song a far more aggressive feel while preserving the beats-and-samples feel and ominous sense of drama. ‘Interstellar Threat’ meanwhile appears in three forms; a forceful trance-like version, a heavy industrial mix with machine-gun guitar riffs sand a peculiar, almost acid house take on the track; dancey; but do people dance to this kind of thing? ‘The New Era of Immortality’ is in a similar vein while ‘Insects Intrude/The War Against the Bugs’ is an almost Deathstars-like metal disco track but far heavier and without the goth posturing. Interesting and sometimes powerful stuff, not just for Kvarforth completists."

Will Pinfold 4/5

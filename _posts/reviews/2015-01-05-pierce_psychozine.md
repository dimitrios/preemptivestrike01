---
layout: posts
title:  '"Pierce..." on Psychozine Magazine'
date:   05-01-2015
categories: Review
permalink: /reviews/pierce-on-psychozine-magazine.html
---
<img src="/images/reviews/9553459.jpg" style="margin-top: 5px; margin-bottom: 10px; margin-left: 0px; margin-right: 10px; border-width:1px;padding:3px; max-width:100%">


> "PreEmptive Strike 0.1 to projekt pochodzący z Grecji. Jest on czymś co powinno trafić w gusta tych wszystkich, którym dźwięki takich hord jak G.G.F.H. czy chociaż Front Line Assembly przyprawiają o szybsze bicie serca. Na materiale, „Pierce Their Husk” ( wydanym pod skrzydlami Odium Records ) usłyszymy więc skomasowany soniczny terror, bardzo wysokich lotów. ..."


More: <http://www.psychozine.eu/modules.php?name=Reviews&rop=showcontent&id=3250>

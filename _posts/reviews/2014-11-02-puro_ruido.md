---
layout: posts
title:  '"Pierce Their Husk" by Puro Ruido'
date:   02-11-2014
categories: Review
permalink: /reviews/pierce-their-husk-by-puro-ruido.html
---
<img src="/images/reviews/690672_orig.jpg" alt="Picture" style="width:100%;max-width:300px">

(In Spanish)

"Primera edición de este sello nacido como subdivisión de Odium Records. Por lo que me cuentan, Sonic Hell Recs se dedicará exclusivamente a lanzar discos de Dark Ambient, Ritual, Industrial, Dark Electro y similares. A juzgar por esta, la primera edición del sello, además de lanzar discos dentro de esos estilos, también apuntan a lanzar buenos discos. No es que el álbum de PES 0.1 sea una maravilla; de hecho, no lo es. Pero entretiene, incita a bailar y a sacudir la cabeza de igual manera, y muestra una faceta que al menos yo desconocía de estos músicos griegos." ...

More: <http://puroruido.blogspot.com.ar/2014/11/preemptive-strike-01-pierce-their-husk.html>

---
layout: posts
title:  '"Pierce..." by Orthodox Black Metal'
date:   02-11-2014
categories: Review
permalink: /reviews/pierce-by-orthodox-black-metal.html
---
<img src="/images/reviews/690672_orig.jpg" alt="Picture" style="width:100%;max-width:300px">

By xDemoNx:

"I was always thinking that in some cases EBM can get quite close regarding the atmosphere or even the feeling that Black Metal puts out. The surprise was big when Shadow of Odium Records told me that he released by his label the new EP of the Greek Preemptive Strike 0.1. The band exists since 2002, it consists of two members and it has released until now three albums. Pierce their husk is their third release and it contains seven compositions of overall duration of about half an hour. As you might have already understood, here we have a band of clear EBM with some industrial elements."

More: <http://www.orthodoxblackmetal.com/preemptivestrike0.1-piercetheirhuskep2014.php>

---
layout: posts
title:  '"Talos" by Brutal Resonance'
date:   22-04-2012
categories: Review
permalink: /reviews/talos-by-brutal-resonance.html
---
<img src="/images/reviews/9095967_orig.png" alt="Picture" style="width:100%;max-width:684px">

<div style="display:block;font-size:90%"></div> </div></div>  <div class="paragraph" style="text-align:left;">
  <span style="text-decoration:none; font-style:normal; font-weight:400; color:rgb(255, 255, 255); ">
    <span style="text-decoration:none; font-style:normal; font-weight:400; color:rgb(255, 255, 255); ">
      <span style="text-decoration:none; font-style:normal; font-weight:400; color:rgb(255, 255, 255); ">
        Sometimes, calling an album a giant undertaking can be taken literally.  An album of this ambition could
        often disappear into the miasma, and for  most bands, would simply be too much. For PreEmptive Strike 0.1,
        this  is an average day in the office. <br><br>More: <a href="http://www.brutalresonance.com/review/preemptive-strike-01-talos/">http://www.brutalresonance.com/review/preemptive-strike-01-talos/</a>
        <br><br>

Nick's rating: 9.5 (<span style="">amazing) - Album of the year for 2012</span>

---
layout: posts
title:  '“Planet Eradicated” of "Eternal Masters" on Talking to Ghosts'
date:   17-10-2015
categories: Review
permalink: /reviews/planet-eradicated-of-eternal-masters-on-talking-to-ghosts.html
---
<img src="/images/logos/talking-to-ghosts_orig.png" alt="Picture" style="width:auto;max-width:100%">


> There are many times when I go into a release knowing full well that it may not be for me - having heard recently that guitars were added to the lineup, or that a traditionally pretty bland electronic artist is doing more traditionally bland things - and this was definitely one of those times. Preemptive Strike 0.1 is an artist that I generally like, but the concept for this album has been done really poorly a bunch of times. However! What sets this album apart from most of the previous efforts to bring metal and industrial into a true merriment is that Preemptive Strike 0.1 went out and recruited a bunch of established metal bands that had very distinct styles and just let them run with the track (from what I understand and can conceptualize from the music). It works effortlessly and seamlessly. With this Wolfheart collaboration especially, so many of the elements are perfectly placed within the established style. The synth elements aren’t too loud or crazy and the quality is really high. I am pleasantly surprised and really congratulate the Preemptive Strike 0.1 team for getting this release together.

<http://www.talkingtoghosts.com/2016/10/17/poltergeists-week-of-october-17-2016>

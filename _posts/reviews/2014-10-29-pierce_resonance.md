---
layout: posts
title:  '"Pierce..." by Brutal Resonance'
date:   29-10-2014
categories: Review
permalink: /reviews/brutal-resonance.html
---
<img src="/images/reviews/8136354_orig.png" alt="Picture" style="width:100%;max-width:693px">

It was quite odd to see that Odium Records, an underground label primarily focused on extreme black metal, would subsequently release an album by [PreEmptive Strike 0.1](http://www.brutalresonance.com/artist/preemptive-strike-01/), the dark electro and EBM Greek hybrid from Jim 'The Blaster' and Cryon. However, after learning that the label had started a new division, things became a little more clear cut. From the womb of the black metal record label, Sonic Hell Records has been born, which will evidently capture the talent of dark ambient, ritual, industrial, and dark electro acts. And, they sure did themselves a fine job in nabbing up Preemptive's lovely groovy minds as their first release.

More: <http://www.brutalresonance.com/review/preemptive-strike-01-pierce-their-husk/>

Steven's rating: 8.0 (great)

---
layout: posts
title:  '"Epos-v" on Sideline'
date:   12-07-2015
categories: Review
permalink: /reviews/epos-v-on-sideline.html
---
<img src="/images/reviews/6341522.jpg" style="margin-top: 5px; margin-bottom: 10px; margin-left: 0px; margin-right: 10px; border-width:1px;padding:3px; max-width:100%">

> "Preemptive Strike 0.1 has released their 5th full length. This band operating from the beautiful Crete Island started as a dark-electro formation, but progressively experimented with different genres. They walked on a metal-driven path at their previous EP (released by Sonic Hell Records) while their electronic sound got slowly infiltrated by elements of Greek folk music..."

Rank: 8

More: <http://www.side-line.com/preemptive-strike-0-1-epos-v-cd-album-infacted-recordings/>

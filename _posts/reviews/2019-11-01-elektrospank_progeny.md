---
layout: posts
title:  "Progeny Of The Technovore on Elektrospank"
date:   01-11-2019
categories: Review
permalink: /reviews/elektrospank.html
---

# Review of "Progeny Of The Technovore"

From **Akill**:

> If you 've read the previous article here in our magazine, you'd probably noticed that I said I am happy and proud for the Greek electro, industrial scene for two reasons. Well, here is the second one. PreEmptive Strike 0.1 released their 7th full length album on October 25th. 

> "Progeny Of The Technovore" is another step forward for the famous harsh electro project from Greece. Perhaps their most powerful album throughout those years, PES 0.1 include here a number of new elements. As George Klontzas has already made clear, in "Progeny Of The Technovore", they move their harsh EBM style into the borders of Goa Trance. The result is a unique uptempo harsh industrial and dark electronic collection of songs. Everyone of the 12 tracks in the album combines powerful elements both from the EBM and Trance style.

> Strong and powerful beats with synth melodies and elements, combined with the aggression of the band's vocals. Again remain focused in their sci fi themes they increase their robotic and vocoder effects in their already agressive, harsh voice.

> Same ttled track "Progeny Of The Technovore" is the most representative example of the two genres PES 0.1 have bring together in the album. I would also mention "Xenotheological Exodeity" for the powerful beats and the trance-oriented synths. "Tactical Nuke Incoming" is one of the tracks I would love to listen to an industrial dancefloor. Absolute power.

> "Progeny Of The Technovore" was mixed and mastered by legendary dark-electro engineer Dimitris N Douvras (CYGNOSIC, SIVA SIX, IAMBIA) at his Lunatech Sounds Studio facility in Greece, and with sepia-toned sleeve artwork from Vlad McNeally. 

> Check PreEpmtive Strike 0.1 new album on their bandcamp page and get your copy!

Link: <http://www.elektrospank.com/news/28-flash/192-preemptive-strike-0-1-7th-full-length-album-progeny-of-the-technovore>


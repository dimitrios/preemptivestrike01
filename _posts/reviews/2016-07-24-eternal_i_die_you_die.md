---
layout: posts
title:  '"Eternal Masters" on I die You Die mini review'
date:   24-07-2016
categories: Review
permalink: /reviews/july-24th-2016.html
---
<img src="/images/logos/idieyoudie-logo_1_orig.png" alt="Picture" style="width:auto;max-width:100%">

> "Most attempts to blend black metal and industrial are straight trash, no lie. This we dig though, as Greek electro-industrialists Preemptive Strike 0.1 team up with Swiss KVLTers Borgne, for what turns into a pretty damn fine blend of programming, hell-scorched blastbeats and guitars. PS0.1 are actually doing a whole EP of these metal band collabs for DWA, complete with limited triple 7″. Strong stuff, made stronger by how lousy most attempts to do something similar turn out."

Original link: <http://www.idieyoudie.com/2016/07/tracks-july-18th-2016/>

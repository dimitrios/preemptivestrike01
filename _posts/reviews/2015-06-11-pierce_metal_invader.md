---
layout: posts
title:  '"Pierce..." on Metal Invader'
date:   11-06-2015
categories: Review
permalink: /reviews/pierce-on-metal-invader.html
---
> "When this quite interesting disc reached my hands I didn’t know what to expect. I like electronic sound and the right combination of metal and electronic music usually leads to either a sound orgasm or a flop, suitable only as a coaster for one’s coffee mug. ... "

More: <http://metalinvader.net/preemptive-strike-0-1-pierce-their-husk/>

Rating: 3.5/6

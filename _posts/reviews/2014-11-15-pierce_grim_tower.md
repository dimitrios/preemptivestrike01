---
layout: posts
title:  '"Pierce..." in The Grim Tower'
date:   15-11-2014
categories: Review
permalink: /reviews/pierce-in-the-grim-tower.html
---
<img src="/images/logos/grim_tower.jpg" style="margin-top: 5px; margin-bottom: 10px; margin-left: 0px; margin-right: 10px; none; max-width:100%">

> "As part of a new label called Sonic Hell Records (dealing with dark ambient, ritual, industrial and dark electronic albums) this 7″ release from dark electronic veterans Preemptive Strike 0.1 (dark electronic fans should be well aware of them, they have had four albums to their credit and some of those were released on electronic/industrial/goth heavyweight Metropolis Records) contains several compilations from members of the black metal scene, to create the sort of electronic black metal release that even I myself have given a shot ... "

More: <http://thegrimtower.com/review-preemptive-strike-0-1-pierce-husk-7/>

---
layout: posts
title:  '"Pierce..." on Side line magazine'
date:   06-01-2015
categories: Review
permalink: /reviews/pierce-on-side-line-magazine.html
---
<img src="/images/logos/side_line.gif" style="margin-top: 5px; margin-bottom: 10px; margin-left: 0px; margin-right: 10px; border-width:1px;padding:3px; max-width:100%">

> "While PreEmptive Strike 0.1 is actually working on a new album, this EP is a kind of very special break and a kind of look back at the “T.A.L.O.S.”-album released in 2012. We all know this duo for their harsh and melodic dark-electronic music, but they also like metal music so this is what this EP is all about. ...
>
> Rate: (DP:8)DP."

More: <http://www.side-line.com/reviews_comments.php?id=51797_0_17_0_C>

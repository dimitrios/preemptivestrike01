---
layout: posts
title:	"Review of Progeny on side-line.com and Inferno Sound Diaries"
date:	15-02-2020
categories: Review
permalink: /reviews/side_line_com_progeny.html
---

# Review of "Progeny of the Technovore" on side-line.com / InfernoSoundDiaries

**Background/Info**: Hailing from the beautiful island of Crete (Greece), PreEmptive Strike 0.1 is back on track. George Klontzas, Jim The Blaster and Yiannis Chatzakis unleashed their seventh official album to date, offering twelve new songs to their fans.

**Content**: I didn’t exactly know what to expect from this announced sonic symbiosis between body-music and goa-trance, but it rapidly appears PreEmptive Strike 0.1 has accomplished their most powerful album to date. It’s a rough and merciless production driven by astonishing rhythms, carrying melody lines and harsh vocals. There’s no trace of a break, all songs hold on to the devilish cadence. I also noticed a few cuts with some extra diversity like the injecting of filtered, acid like sequences.

**Pluses**: I’ve been never disappointed by PreEmptive Strike 0.1, but I never expected they would ever reach this level. This is without a shadow of a doubt their best album to date. They lost a bit of their traditional/folk input, which was characteristic for the early work, but they replaced it with such an overwhelming power. The tracks are also masterfully accomplished with heavy rhythms, carrying leads, elevating choruses and extra little sound treatments. The song “Tactical Nuke Incoming” is without a shadow of a doubt a dancefloor stomper! “Global Insurgency” is another great song.

**Minuses**: I don’t see real minus points to mention; the production of the vocals could maybe a bit more diversified, but I’m not complaining.

**Conclusion**: This album makes me instantly think of ‘dark-electronics on acid’! This is without a shadow of a doubt the band’s opus magnum and still one of my favorite albums from 2019!

**Best songs**: “Tactical Nuke Incoming”, “Global Insurgency”, “Doppelgangers”, “Sunder Every Atom”, “Constriction Process”.

**Rate**: (9).

**Links**:

- <https://www.side-line.com/preemptive-strike-0-1-progeny-of-the-technovore-album-digital-world-audio/>
- <https://www.facebook.com/InfernoSoundDiaries/photos/a.1515526255393544/2615156522097173/?type=3&theater>

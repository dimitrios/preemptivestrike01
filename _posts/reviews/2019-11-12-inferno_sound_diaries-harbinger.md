---
layout: posts
title:  "Harbinger on InfernoSoundDiaries"
date:   12-11-2019
categories: Review
permalink: /reviews/inferno_sound_diaries_harbinger.html
---

# Review of "Harbinger"

**Background/Info**: “Harbinger” brings us back to PreEmptive Strike 0.1 latest album “Through The Astral Cold” (released in 2017). It’s the second EP bringing us back to this album while it also features three complete new songs. The EP is also available as a very limited CD and vinyl format.

**Content**: If you know the sound and influences of the Crete formation “Harbinger” will not really surprise you. The EP opens with the “Single Edit” from the title track, which stands as the perfect fusion between EBM and dark-electronics. The new songs are worthy of examination and especially “The Era Of Entomocracy”. PreEmptive Strike 0.1 has this talent to create a sonic bridge between EBM and dark-electro and that’s precisely what this work stands for.

**Plus**: I like this kind of EP’s featuring 4 different cuts. The title track is a late return to the album “Through The Astral Cold” while the new songs feature an interesting interlude to the in the meantime new full length “Progeny Of The Technovore”. The power of the songs and their elaboration remain characteristic for this band, which still deserves more recognition.

**Minus**: Even if PreEmptive Strike 0.1 is not an emulation of other groups, the sound remains a bit cliché and sometimes predictable, but you don’t hear me complaining.

**Conclusion**: PreEmptive Strike 0.1 became a warrant for qualitative dark EBM. Up to their new album now!

**Best songs**: “The Era Of Entomocracy”, “Harbinger – Single Edit”, “Teratogenesis”.

**Rate**: (8).

Link: <https://www.facebook.com/InfernoSoundDiaries/photos/a.1515526255393544/2529048290707997>

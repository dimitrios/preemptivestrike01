---
layout: posts
title: "Defence Readiness : Condition 1 in Side reshimotohu.com"
date: 12-01-2024
categories: Review
permalink: /reviews/reshimotohu_defence_readiness_condition_1.html
---

Defence Readiness : Condition 1 in reshimotohu.com

Link: <https://www.reshimotohu.com/reviews#/the-note-of-chaos-notes-albums-of-2023>

Original text in Hebrew, translation here from ChatGPT4:

My long-time readers surely remember the interview I conducted with the band members a few years ago. It was perhaps the first interview I conducted for "Tehu Notes," and Preemptive Strike (hereinafter PES) were also at the beginning of their journey towards international fame. Therefore, this choice was accompanied by a feeling similar to the one I had when I chose "To Lunar Poetry" as one of my 2022 albums. Like Nokturnal Mortum, I got to know PES roughly from the start of their journey, and in this case, I even had a semi-personal contact with some of its members. I feel like I've accompanied them from their days as an anonymous underground band, until that moment when it became clear to everyone that they are at the "next level" of the game - both in terms of the quality of their work and the buzz they create in the scene. Although it wasn't my personal success, witnessing such a closure of a circle, from a journey filled with challenges that almost led to the band's dissolution, fills me with a sense of satisfaction. I'm truly happy for them and very excited - because their creation that stands before you is perhaps the magnum opus of these guys, although I have a feeling they will surpass themselves.

When you listen to this album, the first thing you may notice is the intense listening experience. This is not music that can be "pushed into the background": it hardly has dull moments and captures the center of attention as long as it plays. In doing so, it captures the listener inside the special world that this ensemble repeatedly creates in its works. This world is always a futuristic battlefield, where the souls operating in it are partly mythological heroes, partly mortal warriors. A world that treads on the seam line between the mythical and mystical and the futuristic science fiction, between epic and space opera. I want to enter this world as one of the heroes and stay there forever - even if the unfolding story is like the story in the album before us.

Is there really a story in it? Well, I'm not sure that was the original intention of our dear poets, but this time I can't help but think that it's a kind of concept album, with a very dark story gradually unfolding before the listener. It is a story of a country's (or a whole planet's?) entry into war, beginning with a spaceship whose crew perished under unclear circumstances ("Stellar Castaway"), the takeover of a distant base by a hostile force ("The Base was Overrun"), and mass panic in the streets ("Pause in Chaos\Stop the Madness"), ending in a state of readiness for nuclear war ("State of War").

The musical tone, on melancholic minor scales and somber severities, manages to convey the same sense of the gravity of the situation that accompanies the story. A feeling that was, unfortunately, too familiar to me from our recent times. Yet, out of this heaviness rises a rhythm, exploding inside the bloodstream in the form of adrenaline, readiness for battle.

It is an invitation to the dance floor - if the only remaining dance floor is the burning and bleeding ground of the battlefield, both external and internal. These are sounds that propel forward, from an almost crouched and submissive position to standing in valor, with your heart locked on your god and your eyes on your foes, and every waking hour of your day dedicated to the downfall of your adversaries.

There were times in human history when the musician's role in the battlefield was no less important than that of the warrior. I remember a relief I saw at the Pergamont Museum in Berlin, depicting Assyrian soldiers accompanied by a procession of musicians. I can imagine how these musicians managed to infuse courage and hope into the hearts of the warriors beside them, and terror and defeatism into the enemy's armies. In this sense, they almost fulfilled the role of a priest summoning the gods of war to the battlefield. I don't know how common this practice is today in the physical battlefield, but I am sure and confident that the battlefield of the spirit has a splendid corps of musicians, infusing valor in their brothers in arms and breaking the spirit of their enemies with the sounds of genres such as martial industrial, metal, and combatant EBM, as only PES knows how to do.

And if we are dealing with the wars of the spirit, it is certainly worth mentioning the piece that, in my opinion, seals the narrative part of the album, namely "Beyond Reality." The feeling that arises from it is a strange combination between the sadness mixed with hope of the piece that closes the split between Toroidh and Arditi, and the dramatic theatricality of a classic James Bond movie opening song. The song is a catharsis, arising at the end of a journey that began in hell on the grounds of "State of Readiness 1," continued with the hardships of the internal hell ("I let the beast enter my soul, now what have I become?


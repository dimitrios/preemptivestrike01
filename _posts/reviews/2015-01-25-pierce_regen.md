---
layout: posts
title:  '"Pierce..." on RegenMag'
date:   25-01-2015
categories: Review
permalink: /reviews/pierce-on-regenmag.html
---
<img src="/images/reviews/79924_orig.png" style="margin-top: 5px; margin-bottom: 10px; margin-left: 0px; margin-right: 10px; none; max-width:100%">

Picture

> "PreEmptive Strike 0.1 teases us with this EP while the band works in the studio, and it shows that the talented duo can mix more black metal into their “dark electro” style."

Stars: 3.5

More: <http://regenmag.com/reviews/preemptive-strike-0-1-pierce-husk-ep/>

---
layout: posts
title:  '"Pierce..." on Industrial Strength Nightmares'
date:   12-12-2014
categories: Review
permalink: /reviews/pierce-on-industrial-strength-nightmares.html
---
<img src="/images/logos/industrial_strength_nightmares.png" style="margin-top: 5px; margin-bottom: 10px; margin-left: 10px; margin-right: 10px; border-width:0; max-width:100%">

> "... For those who aren't aware, after the release of the fantastic T.A.L.O.S. PES 0.1 embarked on a remix project with black metal artists AD HOMINEM. Whilst reaction was mixed, it seems that experience has changed the PES 0.1 perspective on music. This is clearly evident on the EP, with the remixes of tracks being done by various black metal bands. ..."

By Jarrad Stock, rating: 4/5

More: <http://www.isnradio.com/articles/review-preemptive-strike-pierce-their-husk-8498394288>

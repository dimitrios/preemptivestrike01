---
layout: posts
title:  '"Epos-v" on Brutal Resonance'
date:   26-06-2015
categories: Review
permalink: /reviews/epos-v-on-brutal-resonance.html
---
<img src="/images/reviews/962995.jpg" style="margin-top: 5px; margin-bottom: 10px; margin-left: 0px; margin-right: 10px; border-width:1px;padding:3px; max-width:100%">

> "I think it's safe for me to say that a lot of fans of Preemptive Strike 0.1 have been waiting for an album like 'EPOS V' ever since 'T.A.L.O.S.' came to release."

By Steven Gullotta, 8.5 / 10

More: <http://brutalresonance.com/review/preemptive-strike-01-epos-v/>

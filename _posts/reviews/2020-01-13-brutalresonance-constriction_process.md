---
layout: posts
title:	"Video for Constriction Process in Brutal Resonance"
date:	13-01-2020
categories: Review
permalink: /reviews/brutalresonance_constriction_process.html
---

# Video for "Constriction Process" in Brutal Resonance

Dark electro project Preemptive Strike 0.1 has released a brand new lyric video for their song 'Constriction Process'. The song comes off of the band's most recent album "Progeny Of The Technovore" which released on October 25th, 2019 via DWA. You can stream / purchase the album HERE and you can check out the video directly below:

<iframe width="560" height="315" src="https://www.youtube.com/embed/tGDgMFQkxPI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Link: <http://brutalresonance.com/news/preemptive-strike-01-releases-new-lyric-video-for-constriction-process>


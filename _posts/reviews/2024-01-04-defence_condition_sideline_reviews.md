---
layout: posts
title: "Defence Readiness : Condition 1 in Side Line Reviews"
date: 04-01-2024
categories: Review
permalink: /reviews/sideline_reviews_defence_readiness_condition_1.html
---

Defence Readiness : Condition 1 in Side Line Reviews

**Review**:

**Background/Info**: After having released five albums on Infacted Recordings and next three albums on Digital World Audio PreEmptive Strike 0.1 moves back to Infacted to unleash their ninth full length to date. The work comes four years after the amazing “Progeny Of The Technovore”-album. George Klontzas left the band and was replaced by Yiannis Chatzakis (Degenerated Sequences) who was a live member. Jim ‘The Blaster’ remains the band’s singer. 

**Content**: The album sounds a bit like moving back to the ‘roots’ of the band; a perfect sonic match between elaborated EBM and Dark-Electronic atmospheres. You however will notice Technoid elements as well and even a few Cinematic parts plus a cover version of a Manowar-song. Kriistal Ann (Paradox Obscur) and Cleopatra Caido have been invited to sing each on one song. Last but not least there’s also a remix accomplished by Seven Trees. 

This album isn’t exactly a new beginning but there’s a different approach in the writing process and injected influences. I however think that there’s a chemistry between both members and the move back to the roots of the band will be noticed by fans. The songs are elaborate and I like the uplifting and somewhat Technoid sequences running through most of the songs -but especially during the second half of the album. “Pause In Chaos/Stop The Madness” is a true masterpiece but I also have to mention “State Of War – Defcon 1 Edit” and “Resurrective Hunger” featuring the emblematic vocals of Kriistal Ann. “Beyond Reality” featuring Cleopatra Caido sounds more Cinematic but is another attention grabber. 

I still consider “Progeny Of The Technovore” as the band’s magnum opus so it’s not an easy thing moving on after such a blasting piece of music. Some tracks are a little less hesitant but I’m confident for the further evolution of the Greek duo. 

**Conclusion**: PreEmptive Strike 0.1 is back and still stands for attractive EBM. 

**Best songs**: “Pause In Chaos/Stop The Madness”, “Resurrective Hunger”, “State Of War – Defcon 1 Edit”,  “Beyond Reality”, “Lunacy Stimulator”, “Marauders From Earth”. 

Link: <https://www.facebook.com/InfernoSoundDiaries/posts/pfbid026jibPDhijQRGDaCH87NNVUF7zvzAqfUct8unwDxfSULxnQtguC83cxiNbpHLx8Ndl>

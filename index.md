---
# Feel free to add content and custom Front Matter to this file.

layout: home
---

# {{ site.title }}

Preemptive strike 0.1 was founded on the Greek island of Crete in 2002 with the purpose of creating powerful, melodic EBM music. Since 2019 the band consists of two members: Jim the Blaster, and Yiannis Dseq.

They have evolved to incorporate eastern elements and traditional Greek instruments as well as often experimenting and collaborating with artists from the extreme metal scene.

On 2023 band returned to Infacted Recordings having ready their next album to be released within 2023. Up to now they have released two singles: "[Ressurective Hunger](https://infactedrecordings.bandcamp.com/album/ressurective-hunger)" and "[Marauders from Earth](https://infactedrecordings.bandcamp.com/album/marauders-from-earth)".

To date they have released 7 critically-acclaimed, full-length albums, 4 EPS a 7” single and 1 tape.

{% include product_box.html %}

---

{% comment %}
[Digital World Audio](http://www.digitalworld.audio/) press release for "Progeny Of The Technovore":

7 is a number considered lucky by many – and lucky indeed it is for you, if you’re a fan of high-tempo dark-electro/aggrotech… because today we are over the moon to announce the stunning seventh album of PREEMPTIVE STRIKE 0.1!

PES0.1’s second full-length for DWA (if we exclude the experimental metal mini-albums “Eternal Masters 1 + 2”) is nothing less than a 12 track extravaganza of full-on hammering electro-industrial aimed foursquare at dancefloor demolition.

As publicly hinted at on social media in the run-up to this announcement, for “Progeny Of The Technovore” songwriter George Klontzas wanted to incorporate a style intrinsically connected in its origins to EBM yet seldom rewired into it: Goa Trance.

The results speak for themselves: “Progeny Of The Technovore” is simply relentless and irresistible, futuristic and in your face. PREEMPTIVE STRIKE 0.1 has never sounded more up-tempo and oozing energy from every pore.

Mixed and mastered as ever by legendary dark-electro engineer Dimitris N Douvras (CYGNOSIC, SIVA SIX, IAMBIA) at his Lunatech Sounds Studio facility in Greece, and with sepia-toned sleeve artwork from Vlad McNeally, “Progeny Of The Technovore” comes out late October both digitally and in limited edition gatefold digipak with lyric booklet.
credits

Released on 25th of October 2019.
{% endcomment %}

## ![Band Image](/images/band_2023.jpg)

### Read exclusively on this site:

- [Harbinger Release Interview](/interviews/harbinger.html),
- [Interview with DSeq](/interviews/yiannis-dseq-on-preemptive-strike.html), PS01's latest member,
- [Interview with Cryon](/interviews/cryon-on-preemptive-strike.html), PS01's founding member,
- [Interview with Blaster](/interviews/blaster-on-preemptive-strike-part-1.html), PS01's founding member, first part.
- [Interview with Blaster](/interviews/blaster-on-preemptive-strike-part-2.html), PS01's founding member, second part.
